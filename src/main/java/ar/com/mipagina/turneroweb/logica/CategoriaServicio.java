package ar.com.mipagina.turneroweb.logica;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.mipagina.turneroweb.modelo.Categoria;
import ar.com.mipagina.turneroweb.persistencia.CategoriaRepositorio;

@Service
public class CategoriaServicio {

	
	@Autowired
	private CategoriaRepositorio repositorio;
	
	public Categoria getCategoria(Integer id) {
		return repositorio.getOne(id);
	}
	
	public List<Categoria> listarTodos() {
		return repositorio.findAll();
	}

	public Categoria guardar(Categoria c) {
		return repositorio.save(c);
	}

	public Categoria actualizar(Categoria c) {
		if (c.getId() == null) {
			throw new RuntimeException("Sin referencia del objeto.");
		}
		return repositorio.save(c);
	}

	public void eliminar(Integer id) {
		repositorio.deleteById(id);
	}

	public List<Categoria> listarTodosPorNombreYDescripcion(String nombre, String desc) {
		return repositorio.findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(nombre, desc);
	}

	public List<Categoria> listarTodosPorNombreODescripcion(String nombre, String desc) {
		return repositorio.findByNombreContainingIgnoreCaseOrDescripcionContainingIgnoreCase(nombre, desc);
	}

	
}
