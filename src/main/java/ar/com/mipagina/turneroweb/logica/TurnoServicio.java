package ar.com.mipagina.turneroweb.logica;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.mipagina.turneroweb.modelo.Turno;
import ar.com.mipagina.turneroweb.persistencia.TurnoRepositorio;


@Service
public class TurnoServicio {

	@Autowired
	private TurnoRepositorio repositorio;
	@Autowired
	private EstadoServicio estadoServicio;
	@Value("${config.turnero.horaInicio}")
	private String horaInicio;
	@Value("${config.turnero.horaFin}")
	private String horaFin;

	public Turno getTurno(Integer id) {
		return repositorio.getOne(id);
	}
	
	public Page<Turno> listarTodos(Pageable pagina) {
		return repositorio.findAll(pagina);
	}

	public Page<Turno> listarTurnoPorNombreApellido(String nombreApellido, Pageable pagina) {
		return repositorio.findByNombreApellido(nombreApellido, pagina);
	}

	public Page<Turno> listarTurnoPorFecha(LocalDate fecha, Pageable pagina) {
		return repositorio.findByFecha(fecha,pagina);
	}

	public Turno guardar(Turno turno) {
		if (!validarHorario(turno.getFecha(), turno.getHora())) {
			throw new RuntimeException("El inicio de actividades es a las "+this.horaInicio+"hs.");
		} else if (!validarTurnoExistente(turno)) {
			throw new RuntimeException("Ya existe un turno para el horario solicitado.");
		}
		turno.setEstado((estadoServicio.obtenerEstado("Pendiente")).get(0));
		return repositorio.save(turno);
	}

	public Turno actualizar(Turno turno) {
		if (!validarHorario(turno.getFecha(),turno.getHora())) {
			throw new RuntimeException("El inicio de actividades es a las "+this.horaInicio+"hs hasta las "+this.horaFin+"hs.");
		} else if (!validarTurnoExistente(turno)) {
			throw new RuntimeException("Ya existe un turno para el horario solicitado.");
		}
		return repositorio.save(turno);
	}

	private boolean validarTurnoExistente(Turno turno) {
		List<Turno> turnos = repositorio.buscarTurnoSolapado(turno.getFecha(),turno.getHora().minusMinutes(15).plusSeconds(1),turno.getHora().plusMinutes(15).minusSeconds(1));
		if (turnos.isEmpty()) {
			return true;
		}else {
			for(int i=0;i<turnos.size();i++) {
				if (!(turno.getId().equals((turnos.get(i).getId())))) { //Validar si el turno es "Eliminado"
					return false;
				}
			}
		}
		return true;
	}

	public boolean validarHorario(LocalDate fecha, LocalTime hora) {
		LocalTime hsInicio = LocalTime.parse(horaInicio);
		LocalTime hsFin = LocalTime.parse(horaFin);
		if (fecha.isBefore(LocalDate.now()) || (fecha.isEqual(LocalDate.now()) && hora.isBefore(LocalTime.now())) || hora.isBefore(hsInicio) || hora.isAfter(hsFin)) {
			return false;
		} else {return true;}
	}

	public Turno eliminar(Turno turno) {
		if (!validarHorario(turno.getFecha(),turno.getHora())) {
			throw new RuntimeException("El turno que desea eliminar ya ha ocurrido.");
		} 		
		turno.setEstado((estadoServicio.obtenerEstado("Eliminado")).get(0));
		return repositorio.save(turno);
	}	
}
