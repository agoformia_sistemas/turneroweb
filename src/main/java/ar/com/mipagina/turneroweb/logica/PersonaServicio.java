package ar.com.mipagina.turneroweb.logica;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ar.com.mipagina.turneroweb.modelo.Persona;
import ar.com.mipagina.turneroweb.persistencia.PersonaRepositorio;

@Service
public class PersonaServicio {

	
	@Autowired
	private PersonaRepositorio repositorio;
	
	public Persona getPersona(Integer id) {
		return repositorio.getOne(id);
	}
	
	public List<Persona> listarTodos() {
		Sort orden = Sort.by(Sort.Direction.ASC, "nombre"); 
		return repositorio.findAll(orden);
	}

	public Persona guardar(Persona p) {
		return repositorio.save(p);
	}

	public Persona actualizar(Persona p) {
		if (p.getId() == null) {
			throw new RuntimeException("Error el objeto no tiene id.");
		}
		return repositorio.save(p);
	}

	public void eliminar(Integer id) {
		repositorio.deleteById(id);
	}

	public Page<Persona> listarTodosFiltradoPorNombre(String nombre, Pageable pagina) {
//		Pageable pagina = PageRequest.of(0, 10);
		return repositorio.findByNombreContainingIgnoreCase(nombre,pagina);
	}	

	public List<Persona> listarTodosFiltradoPorNombreLocalidad(String localidad) {
		return repositorio.findByLocalidad_NombreContainingIgnoreCase(localidad);
	}
	
	public List<Persona> listarTodosFiltradoPorNombreYLocalidad(String nombre, String localidad) {
		return repositorio.findByNombreContainingIgnoreCaseAndLocalidad_NombreContainingIgnoreCase(nombre,localidad);
	}


}
