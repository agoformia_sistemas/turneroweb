package ar.com.mipagina.turneroweb.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.mipagina.turneroweb.modelo.Estado;
import ar.com.mipagina.turneroweb.persistencia.EstadoRepositorio;

@Service
public class EstadoServicio {

	@Autowired
	private EstadoRepositorio repositorio;

	public List<Estado> obtenerEstado(String nombre) {
		return repositorio.findByNombre(nombre);
	}
	
}
