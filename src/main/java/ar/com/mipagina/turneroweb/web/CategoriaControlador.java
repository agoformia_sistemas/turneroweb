package ar.com.mipagina.turneroweb.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.mipagina.turneroweb.logica.CategoriaServicio;
import ar.com.mipagina.turneroweb.modelo.Categoria;

@RestController
@RequestMapping("/categorias")
public class CategoriaControlador {
	
	@Autowired
	private CategoriaServicio servicio;
	
	@GetMapping
	public List<Categoria> listarTodos() {
		return servicio.listarTodos();
	}
	
//	@GetMapping(params = {"nombre","descripcion"})
//	public List<Categoria> listarTodosPorNombreYDescripcion(String nombre, @RequestParam(name = "descripcion") String desc) { //Se puede obviar el @RequestParams colocando el valor del parámetro indicado por URL en el parámetro del método.
//		return servicio.listarTodosPorNombreYDescripcion(nombre,desc);
//	}
	
	@GetMapping(params = {"nombre","descripcion"})
	public List<Categoria> listarTodosPorNombreODescripcion(String nombre, @RequestParam(name = "descripcion") String desc) { //Se puede obviar el @RequestParams colocando el valor del parámetro indicado por URL en el parámetro del método.
		return servicio.listarTodosPorNombreODescripcion(nombre,desc);
	}
	
	@GetMapping(value = "/{id}")
	public Categoria getCategoria(@PathVariable(name="id")Integer id) {
		return servicio.getCategoria(id);
	}
	
	@PostMapping
	public Categoria guardar(@RequestBody Categoria c) {
		return servicio.guardar(c);
	}
	
	@RequestMapping(value= "/{id}", method = RequestMethod.PUT)
	public Categoria actualizar(@RequestBody Categoria c, @PathVariable(name="id")Integer id) {
		return servicio.actualizar(c);
	}

	@RequestMapping(value= "/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id")Integer id) {
		servicio.eliminar(id);
	}
}
