package ar.com.mipagina.turneroweb.web;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.mipagina.turneroweb.logica.TurnoServicio;
import ar.com.mipagina.turneroweb.modelo.Turno;

@RestController
@RequestMapping("/turnos")
public class TurnoControlador {

	@Autowired
	private TurnoServicio servicio;
	
	@PostMapping
	public Turno guardar(@RequestBody Turno turno) {
		return servicio.guardar(turno);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public Turno actualizar(@RequestBody Turno turno, @PathVariable(name="id") Integer id) {
		return servicio.actualizar(turno);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public Turno eliminar(@RequestBody Turno turno, @PathVariable(name="id") Integer id) {
		return servicio.eliminar(turno);
	}
	
	@GetMapping
	public Page<Turno> listarTodos(Pageable pagina) {
		return servicio.listarTodos(pagina);
	}
	
	@GetMapping(value = "/{id}")
	public Turno getTurno(Integer id) {
		return servicio.getTurno(id);
	}
	
	@GetMapping(params = "fecha")
	public Page<Turno> listarTurnoPorFecha(LocalDate fecha, Pageable pagina) {
		return servicio.listarTurnoPorFecha(fecha, pagina);
	}
	
	@GetMapping(params = "nombreApellido")
	public Page<Turno> listarTurnoPorNombreApellido(String nombreApellido, Pageable pagina) {
		return servicio.listarTurnoPorNombreApellido(nombreApellido, pagina);
	}
	
}
