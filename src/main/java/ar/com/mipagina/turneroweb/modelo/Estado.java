package ar.com.mipagina.turneroweb.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@Entity
@NoArgsConstructor
public class Estado {

	@Id
	@GeneratedValue
	private int id;
	@NonNull
	private String nombre;
	private String descripcion;
}
