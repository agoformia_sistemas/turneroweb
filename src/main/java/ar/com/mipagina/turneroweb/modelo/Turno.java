package ar.com.mipagina.turneroweb.modelo;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
public class Turno {

	@Id
	@GeneratedValue
	private Long id;
	@NonNull
	private String nombreApellido;
	@NonNull
	private LocalDate fecha;
	@NonNull
	private LocalTime hora;
	@ManyToOne @NonNull
	private Categoria categoria;
//	private boolean programado;
//	private Integer orden;
	@ManyToOne @NonNull
	private Estado estado;
}
