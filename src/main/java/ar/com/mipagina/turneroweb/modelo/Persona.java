package ar.com.mipagina.turneroweb.modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
public class Persona {

	@Id
	@GeneratedValue
	@NonNull
	private Integer id;
	private String nombre;
	private String apellido;
	private boolean activo;
	@ManyToOne            
	private Localidad localidad;
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;

	public String getNombreApellido() {
		return this.getApellido()+ ", "+this.getApellido();
	}
}
