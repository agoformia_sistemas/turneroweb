package ar.com.mipagina.turneroweb.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
public class Categoria {

	@Id
	@GeneratedValue
	private Integer id;
	@NonNull
	private String nombre;
	private String descripcion;
}
