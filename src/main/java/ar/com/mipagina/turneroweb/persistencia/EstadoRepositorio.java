package ar.com.mipagina.turneroweb.persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.mipagina.turneroweb.modelo.Estado;

@Repository
public interface EstadoRepositorio extends JpaRepository<Estado, Integer>{
	
	List<Estado> findByNombre(String nombre);
}
