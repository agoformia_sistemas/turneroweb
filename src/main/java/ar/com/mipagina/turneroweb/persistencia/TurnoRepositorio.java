package ar.com.mipagina.turneroweb.persistencia;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.com.mipagina.turneroweb.modelo.Turno;

@Repository
public interface TurnoRepositorio extends JpaRepository<Turno, Integer>{

	Page<Turno> findByNombreApellido(String nombreApellido, Pageable pagina);

	Page<Turno> findByFecha(LocalDate fecha, Pageable pagina);

	@Query("Select t from Turno t where t.fecha = ?1 and (t.hora between (?2) and (?3))")
	List<Turno> buscarTurnoSolapado(LocalDate fecha, LocalTime rangoDesde, LocalTime rangoHasta);

}
