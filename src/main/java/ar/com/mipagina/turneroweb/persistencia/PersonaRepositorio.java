package ar.com.mipagina.turneroweb.persistencia;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.com.mipagina.turneroweb.modelo.Persona;

@Repository
public interface PersonaRepositorio extends JpaRepository<Persona, Integer>{

	List<Persona> findByNombre(String nombre);

	List<Persona> findByNombreIgnoreCase(String nombre);

	List<Persona> findByNombreContainingIgnoreCase(String nombre);
	
	@Query("Select p from Persona p where p.nombre like %?1%")
	List<Persona> buscarPorNombre(String nombre);
	
	List<Persona> findByNombreContainingIgnoreCaseOrderByNombre(String nombre);	
	Page<Persona> findByNombreContainingIgnoreCase(String nombre, Pageable pagina);
	List<Persona> findByLocalidad_NombreContainingIgnoreCase(String localidad);
	
	List<Persona> findByNombreContainingIgnoreCaseAndLocalidad_NombreContainingIgnoreCase(String nombre, String localidad);
}