package ar.com.mipagina.turneroweb.persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.mipagina.turneroweb.modelo.Categoria;

@Repository
public interface CategoriaRepositorio extends JpaRepository<Categoria, Integer>{

	List<Categoria> findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(String nombre, String desc);
	
	List<Categoria> findByNombreContainingIgnoreCaseOrDescripcionContainingIgnoreCase(String nombre, String desc);
}
